package es.jddavalos.rubik;

public class Cara {
	
	private Color[][] datos;
	
	private Color centro;
	
	private int alto;
	
	private int ancho;
	
	public Cara(Color centro, int alto, int ancho) {
		this.alto = alto;
		this.ancho = ancho;
		this.centro = centro;
		this.datos = new Color[this.alto][this.ancho];
		for (int fila = 0; fila < datos.length; fila++) {
			for (int columna = 0; columna < datos.length; columna++) {
				this.datos[fila][columna] = this.centro;
			}
		}
	}
	
	public Color[][] getDatos() {
		return this.datos;
	}
	
	public void setDatos(int fila, int columna, Color color) {
		if (!isCentro(fila, columna)) {
			this.datos[fila][columna] = color;
		}
	}
	
	private boolean isCentro(int fila, int columna) {
		return (fila == 1 && columna == 1);
	}
	
	/*
	0 AND 0 = 0
	0 AND 1 = 0
	1 AND 0 = 0
	1 AND 1 = 1
	*/
	public boolean caraResuelta() {
		boolean resuelta = true;
		for (int fila = 0; fila < datos.length; fila++) {
			for (int columna = 0; columna < datos.length; columna++) {
				resuelta = resuelta && (this.datos[fila][columna] == this.centro);
				// resuelta &= (this.datos[fila][columna] == this.centro);
				// c++
				// c+=1
				// c=c+1
			}
		}
		return resuelta;
	}

}
