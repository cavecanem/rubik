package es.jddavalos.rubik;

public enum Color {
	
	BLANCO,
	
	VERDE,
	
	AMARILLO,
	
	AZUL,
	
	ROJO,
	
	NARANJA;

}
